import axios from 'axios';

export default class Search {
    constructor(query) {
        this.query = query
    }

    async getResults() {        
        const proxy = 'https://cors-anywhere.herokuapp.com/';
        const key = '9773996447429c7d8fe5acb1bccd3476'
            
        try {
            const res = await axios(`${proxy}https://www.food2fork.com/api/search?key=${key}&q=${this.query}`);
            this.result = res.data.recipes;
        } catch (error) {
            alert(error);
        } 
    }    
}