/**
 * Let's starting code
 */
//console.log("Hello World!!!");

/**
 * Variables and Data types
 */
/*var firstName = 'Jhon';
console.log(firstName);

var lastName = 'Smith';
var age = 28;

var fullAge = true;
console.log(fullAge);

var job;
console.log(job);

job = 'Teacher';
console.log(job);*/

/**
 * Variable Mutation and type coercion
 */
/*var firstName = 'John';
var age = 28;

//Type coercion
console.log(firstName + ' ' + age);

var job, isMarried;
job = 'teacher';
isMarried = false;

console.log(firstName + 'is a ' + age + ' year old ' + job + '. Is he married? ' + isMarried);

// Variable mutation
age = 'twenty-eight';
job = 'driver';

alert(firstName + 'is a ' + age + ' year old ' + job + '. Is he married? ' + isMarried);

var lastName = prompt('What is his last name?');
console.log(firstName + ' ' + lastName);*/

/**
 * Basic operators
 */
/*var now, yearJohn, yearmark;
now = 2018;
var ageJohn = 28;
var ageMark = 33;   

//Math operators
yearJohn = now - ageJohn;
yearMark = now - ageMark;

console.log(yearJohn);

console.log(now + 2);
console.log(now * 2);
console.log(now / 2);

//Logical operators
var johnOlder = ageJohn > ageMark;
console.log(johnOlder);

// typeof operator
console.log(typeof johnOlder);
console.log(typeof ageJohn);
console.log(typeof 'Mark is older that John');
var x;
console.log(typeof x);*/

/**
 * Operator precedence
 */
/*var now = 2018;
var yearJohn = 1989;
var fullAge = 18;

//multiple operators
var isFullAge = now - yearJohn >= fullAge;
console.log(isFullAge);

//grouping
var ageJohn = now - yearJohn;
var ageMark = 35;
var average = (ageJohn + ageMark) / 2;
console.log(average);

//Multiple assigments
x = y = (3 + 5) * 4 - 6;
console.log(x, y);

//More operators
x *= 2;
console.log(x);
x += 10;
console.log(x);
x++;
console.log(x);
x--;
console.log(x);*/

/**
 * Coding chalenge 1
 */
/*var massMark = prompt('What is Mark\'s mass?');
var heightMass = prompt('What is Mark\'s height?');
var massJohn = prompt('What is John\'s mass?');
var heightJohn = prompt('What is John\'s height?');


var bmiMark = massMark / Math.pow(heightMass, 2);
var bmiJohn = massJohn / Math.pow(heightJohn, 2);
console.log("John\'s bmi: " + bmiJohn + "Mark\'s bmi: " + bmiMark);

var markHigherJohn = bmiMark > bmiJohn;
console.log('Is Mark\'s BMI higher than John\'s? ' + markHigherJohn);*/

/**
 * If / else statements
 */
/*var firstName = 'John';
var civilStatus = 'single';

if (civilStatus === 'married') {
    console.log(firstName + ' is married!');
} else {
    console.log(firstName + ' will hopefully marry soon :)');
}

var isMarried = true;
if (isMarried) {
    console.log(firstName + ' is married!');
} else {
    console.log(firstName + ' will hopefully marry soon :)');
}

var massMark = prompt('What is Mark\'s mass?');
var heightMass = prompt('What is Mark\'s height?');
var massJohn = prompt('What is John\'s mass?');
var heightJohn = prompt('What is John\'s height?');


var bmiMark = massMark / Math.pow(heightMass, 2);
var bmiJohn = massJohn / Math.pow(heightJohn, 2);

if (bmiMark > bmiJohn) {
    console.log('Mark\'s BMI is higher than John\'s');
} else {
    console.log('John\'s BMI is higher than Mark\'s');
}*/

/**
 * Boolean logic
 */
/* var firstName = 'John';
var age = 16;

if(age < 13) {
    console.log(firstName + ' is a boy.');
} else if (age >= 13 && age < 20) {
    console.log(firstName + ' is a teenager.');
} else if (age >= 20 && age < 30) {
    console.log(firstName + ' is a young man.');
} else {
    console.log(firstName + ' is a man.')
} */

/**
 * The ternary Operator and Switch Statements
 */
/* var firstName = 'John';
var age = 14;

// Ternary operator
age >= 18 ? console.log(firstName + ' drinks beer.') : console.log(firstName + ' drinks juice.');
 
var drink = age >= 18 ? 'beer' : 'juice'; 
console.log(drink); */

/* if (age >= 18) {
    var drink = 'beer';
} else {
    var drink = 'juice';
} */

// Switch statement
/* var job = 'teacher';
switch (job) {
    case 'teacher':
    case 'instructor': 
        console.log(firstName + ' is a teacher.');
        break;
    case 'driver':
        console.log(firstName + ' is a driver.');    
        break;
    case 'design':
        console.log(firstName + ' is a design.');
        break; 
    default:
        console.log(firstName + ' does something else.');
}

switch (true) {
    case age < 13:
        console.log(firstName + 'is a boy.');
        break;
    case age >= 13 && age < 20:
        console.log(firstName + ' is a teenager.');    
        break;
    case age >= 20 && age < 30:
        console.log(firstName + ' is a young man.');
        break;
    default:
        console.log(firstName + ' is a man');
} */

/**
 * Truthy and Falsy values and equality operators
 */

 // falsy values: undefined, null, 0, '', NaN
 //truthy values: Not falsy values

/*  var height;

 if (height) {
     console.log('Variable is defined');
 } else {
     console.log('Variable has NOT been defined')
 }

 height = 23;

 //equality operators
 if (height == '23') {
     console.log('The == operator does type coercion!');
 } */

 /**
 * Coding chalenge 2
 */

/* var scoreJTeam = 89 + 120 + 103;
var scoreMteam = 116 + 94 + 123;

var averageJTeam = scoreJTeam / 3;
var averageMTeam = scoreMteam / 3;

if (averageJTeam > averageMTeam) {
   console.log('John\'s wins with' + averageJTeam);
} else if (averageMTeam > averageJTeam) {
   console.log('Mark\'s wins with' + averageMTeam);
} else {
   console.log('The same average');
}

scoreJTeam = 140 + 120 + 103;
scoreMteam = 116 + 94 + 123;

averageJTeam = scoreJTeam / 3;
averageMTeam = scoreMteam / 3;

if (averageJTeam > averageMTeam) {
   console.log('John\'s wins with' + averageJTeam);
} else if (averageMTeam > averageJTeam) {
   console.log('Mark\'s wins with' + averageMTeam);
} else {
   console.log('The same average');
}

scoreJTeam = 100
scoreMteam = 100;

averageJTeam = scoreJTeam / 3;
averageMTeam = scoreMteam / 3;

if (averageJTeam > averageMTeam) {
   console.log('John\'s wins with' + averageJTeam);
} else if (averageMTeam > averageJTeam) {
   console.log('Mark\'s wins with' + averageMTeam);
} else {
   console.log('The same average');
}

scoreJTeam = 89 + 120 + 103;
scoreMteam = 116 + 94 + 123;
var scoreMaryTeam = 97 + 134 + 105;

averageJTeam = scoreJTeam / 3;
averageMTeam = scoreMteam / 3;
var averageMaryTeam = scoreMaryTeam / 3;

switch (true) {
    case(averageJTeam > averageMTeam && averageJTeam > averageMaryTeam):
       console.log('Johwins:' + scoreJTeam); 
       break;
    case(averageMTeam > averageJTeam && averageMTeam > averageMaryTeam):
       console.log('Markwins:' + scoreJTeam); 
       break;
    case(averageMaryTeam > averageJTeam && averageMaryTeam > averageMTeam):
       console.log('Marywins:' + scoreJTeam); 
       break;  
    default:
        console.log('draw ');     
} */

/**
 * Functions
 */

 /* function calculateAge(birthYear) {
     return 2018 - birthYear;
 }

 var ageJohn = calculateAge(1990);
 var ageMike = calculateAge(1948);
 var ageJane = calculateAge(1969);

 console.log(ageJohn, ageMike, ageJane);

 function yearsUntilRetirement(year, firstName) {
    var age = calculateAge(year);
    var retirement = 65 - age;

    if (retirement > 0) {
        console.log(firstName + ' retires in ' + retirement + ' years.');
    } else {
        console.log(firstName + ' is already retired.');
    }
    
 }

 yearsUntilRetirement(1990, 'John');
 yearsUntilRetirement(1948, 'Mike');
 yearsUntilRetirement(1969, 'Jane'); */

 /**
  * Function Statements and Expressions
  */

// Function declaration / statement
// function whatDoYouDo(job, firstName) {...}

// Function expression
/* var whatDoYouDo = function(job, firstName) {
    switch(job) {
        case 'teacher':
            return firstName + ' teaches kids how to code';
        case 'driver':
            return firstName + ' drives a cab in Lisbon'
        case 'designer':        
            return firstName + ' designs beautiful websites';
        default:
            return firstName + ' does something else'    
    } 
}

console.log(whatDoYouDo('teacher', 'John'));
console.log(whatDoYouDo('designer', 'Jane'));
console.log(whatDoYouDo('retired', 'Mark')); */

/**
 * Arrays
 */

 //Initialize new array
/* var names = ['john', 'Mark', 'Jane'];
var years = new Array(1990, 1969, 1948);

console.log(names[2]);
console.log(names.length);

// Mutate array data
names[1] = 'Ben';
names[names.length] = 'Mary';
console.log(names);

// Different data types
var john = ['John', 'Smith', 1990, 'teacher', false];

john.push('blue'); // add last in array
john.unshift('Mr.'); // add first in array
console.log(john);

john.pop(); //remove last in array
john.shift(); // remove first in array
console.log(john);

console.log(john.indexOf(23));

var isDesigner = john.indexOf('designer') === -1 ? 'John is NOT a designer' : 'JOhn IS a designer';
console.log(isDesigner);  */

/**
 * Coding Challenge 3
 */

/* var tip = function(value) {

    if(value < 50) {
        return value * 0.2;
    }
    if (value >= 50 && value <= 200) {
        return value * 0.15;
    }        
    else {
        return value * 0.10;
    }
}

var bill = [124, 48, 268];

var tips = [tip(bill[0]), tip(bill[1]), tip(bill[2])];
var total = [bill[0] + tip(bill[0]), bill[1] + tip(bill[1]), bill[2] + tip(bill[2])];

console.log(bill);
console.log(tips);
console.log(total); */

/**
 * Objects and properties
 */

 // Object literal
/* var john = {
    firstName: 'John',
    lastName: 'Smith',
    birthDay: 1990,
    family: ['Jane', 'Mark', 'Bob', 'Emily'],
    job: 'teacher',
    isMarried: false
};

console.log(john.firstName);
console.log(john['lastName']);
var x = 'birthYear';
console.log(john[x]);

john.job = 'designer';
john['isMarried'] = true;
console.log(john);

// new Object syntax
var jane = new Object();
jane.firstName = 'Jane';
jane.birthDay = 1969;
jane['lastName'] = 'Smith';
console.log(jane); */

/**
 * Objects and methods
 */

/* var john = {
    firstName: 'John',
    lastName: 'Smith',
    birthYear: 1990,
    family: ['Jane', 'Mark', 'Bob', 'Emily'],
    job: 'teacher',
    isMarried: false,
    calcAge: function() {
        this.age = 2018 - this.birthYear;
    }
};

john.calcAge();
console.log(john); */

/**
 * Coding Challenge 4
 */

/* var john = {
    fullName: 'John',
    mass: 90,
    height: 1.80,
    bmi: 0,
    calBmi: function() {
        return bmi = (this.mass / Math.pow(this.height, 2));
    } 
};

var mark = {
    fullName: 'Mark',
    mass: 100,
    height: 1.80,
    bmi: 0,
    calBmi: function() {
        return bmi = (this.mass / Math.pow(this.height, 2));
    } 
};

if(mark.calBmi() === john.calBmi()) {
    console.log('The bmis are same');
} else if(mark.calBmi() > john.calBmi()) {
    console.log('The ' + mark.fullName + '\' BMI is bigest');
} else if(mark.calBmi() < john.calBmi()) {
    console.log('The ' + john.fullName + '\' BMI is bigest');
} */

/**
 * Loops and iteration
 */

 // for loop
/* for (var i = 0; i < 10; i++) {
    console.log(i);
}

// i = 0; 0 < 10 true, log i to console, i++
// i = 1; 1 < 10 true, log i to console, i++
// ...
// i = 9; 9 < 10 true, log i to console, i++
// i = 10; 10 < 10 false, exit to loop!

var john = ['John', 'Smith', 1990, 'designer', false];
for (var i = 0; i < john.length; i++) {
    console.log(john[i]);
}

//while loop
var i = 0;
while(i < john.length) {
    console.log(john[i]);
    i++;
}

// continue and break statements
for (var i = 0; i < john.length; i++) {
    if (typeof john[i] !== 'string') continue;
    console.log(john[i]);
}

for (var i = 0; i < john.length; i++) {
    if (typeof john[i] !== 'string') break;
    console.log(john[i]);
}

// Looping backwards

for (var i = john.length - 1; i >= 0; i--) {    
    console.log(john[i]);
} */

/**
 * Coding challenge 5
 */

 var calculateAverage = function (array) {

    var sum = 0;
    for (var i = 0; i < array.length; i++) {
        sum += array[i];
    }

    
    return sum / array.length;
 };


var spent = {
    bill: [124, 48, 268, 180, 42],
    tips: [],
    paid: [],
    calculateTip: function() {
        for(var i = 0; i < this.bill.length; i++) {
            if(this.bill[i] < 50) {
                this.paid.push(this.bill[i] + this.tips.push(this.bill[i] * .2));
            } else if(this.bill[i] >= 50 && this.bill[i] < 200) {
                this.paid.push(this.bill[i] + this.tips.push(this.bill[i] * .15));
            } else if(this.bill[i] >= 200) {
                this.paid.push(this.bill[i] + this.tips.push(this.bill[i] * .1));
            }
        }
    }
};

spent.calculateTip();

console.log(spent.bill);
console.log(spent.tips);
console.log(spent.paid);

var spentMark = {
    bill: [77, 375, 110, 45],
    tips: [],
    paid: [],
    calculateTip: function() {
        for(var i = 0; i < this.bill.length; i++) {
            if(this.bill[i] < 100) {
                this.paid.push(this.bill[i] + this.tips.push(this.bill[i] * .2));
            } else if(this.bill[i] >= 100 && this.bill[i] < 300) {
                this.paid.push(this.bill[i] + this.tips.push(this.bill[i] * .1));
            } else if(this.bill[i] >= 300) {
                this.paid.push(this.bill[i] + this.tips.push(this.bill[i] * .25));
            }
        }
    }
}

spentMark.calculateTip();

console.log(spentMark.bill);
console.log(spentMark.tips);
console.log(spentMark.paid);

var averageTipsJohn = calculateAverage(spent.tips);
var averageTipsMark = calculateAverage(spentMark.tips)

if ( averageTipsJohn > averageTipsMark) {
    console.log('John\'s have the highest tips with ' + averageTipsJohn);
} else if ( averageTipsMark > averageTipsJohn) {
    console.log('Mark\'s have the highest tips with ' + averageTipsMark);
} else {
    console.log('They have the same');
}