// Function constructor

/* var john = {
    name: 'John',
    yearOfBirth: 1990,
    job: 'teacher'
}

var Person = function(name, yearOfBirth, job) {
    this.name = name;   
    this.yearOfBirth = yearOfBirth;
    this.job = job;
}

Person.prototype.calculateAge = function() {
    console.log(2019 - this.yearOfBirth);
}

Person.prototype.lastName = 'Smith';

var john = new Person('John', 1990, 'teacher');
var jane = new Person('Jane', 1969, 'designer');
var mark = new Person('Mark', 1948, 'retired');

john.calculateAge();
jane.calculateAge();
mark.calculateAge();

console.log(john.lastName);
console.log(jane.lastName);
console.log(mark.lastName); */

// Object.create

/* var personProto = {
    calculateAge: function() {
        console.log(2019 - this.yearOfBirth);
    }
};

var john = Object.create(personProto);
john.name = 'John';
john.yearOfBirth = 1990;
john.birth = 'teacher';

var jane = Object.create(personProto, {
    name: { value: 'Jane' },
    yearOfBirth: { value: 1968 },
    job: { value: 'designer' }
}); */

// Primitives vs objects

// Primitives
/* var a = 23;
var b = a;
a = 46;
console.log(a);
console.log(b);


// Objects
var obj1 = {
    name: 'John',
    age: 26
};
var obj2 = obj1;
obj1.age = 30;
console.log(obj1);
console.log(obj2);

// Functions
var age = 27;
var obj = {
    name: 'James',
    city: 'Lisbon'
};

function change(a, b) {
    a = 30;
    b.city = 'San Francisco'
}

change(age, obj);

console.log(age);
console.log(obj.city) */

// Lecture: Passing functions as arguments

/* var years = [1990, 1965, 1937, 2005, 1998];

function arrayCalc(arr, fn) {
    var arrRes = [];
    for (var i = 0; i < arr.length; i++) {
        arrRes.push(fn(arr[i]));
    }
    return arrRes;
}

function isFullAge(el) {
    return el >= 18;
}

function calculateAge(el) {
    return 2016 - el;
}

function maxHeartRate(el) {
    if (el >= 18 && el <= 81)
        return Math.round(206.9 - (0.67 * el));
    else
        return -1;
}

var ages = arrayCalc(years, calculateAge);
var fullAges = arrayCalc(ages, isFullAge);
var rates = arrayCalc(ages, maxHeartRate);

console.log(ages);
console.log(fullAges);
console.log(rates); */

// Lecture: Functions returning functions

/* function interviewQuestion(job) {
    if (job === 'designer') {
        return function (name) {
            console.log(name + ', can you please explain what UX design is?');
        }
    } else if (job === 'teacher') {
        return function (name) {
            console.log('What subject do you teach, ' + name + ' ?');
        }
    } else {
        return function(name) {
            console.log('Hello ' + name + ', what do you do?');
        }
    }
}

var teacherQuestion = interviewQuestion('teacher');
var designerQuestion = interviewQuestion('designer');
teacherQuestion('John');
designerQuestion('John');
designerQuestion('Jane');
designerQuestion('Mark');
designerQuestion('Mike');

interviewQuestion('teacher')('Mark'); */

// IIFE

/* // function game() {
//     var score = Math.random() * 10;
//     console.log(score >= 5);
// }
// game();

(function () {
    var score = Math.random() * 10;
    console.log(score >= 5);
})();

console.log(score)

(function (goodLuck) {
    var score = Math.random() * 10;
    console.log(score >= 5 - goodLuck);
})(5); */

// Closures

/* function retirement(retirementAge) {
    var a = ' years left until retirement.'
    return function (yearOfBirth) {
        var age = 2019 - yearOfBirth;
        console.log((retirementAge - age) + a);
    }
}

var retirementUS = retirement(66);
var retirementGermany = retirement(65);
var retirementIceland = retirement(67);

retirementUS(1990);
retirementGermany(1990);
retirementIceland(1990);

//retirement(66)(1990);

function interviewQuestion(job) {
    return function (name) {
        if (job === 'designer') {
            console.log(name + ', can you please explain what UX design is?');
        } else if (job === 'teacher') {
            console.log('What subject do you teach, ' + name + ' ?');
        } else {
            console.log('Hello ' + name + ', what do you do?');
        }
    }
}

interviewQuestion('teacher')('John'); */

// Bind, call and apply

/* var john = {
    name: 'John',
    age: 26,
    job: 'teacher',
    presentation: function(style, timeOfDay) {
        if (style === 'formal') {
            console.log('Good ' + timeOfDay + ', Ladies and gentlemen! I\'m ' + this.name + ', I \'m a ' + this.job + ' and I\'m ' + this.age + ' years old.');
        } else if (style === 'friendly') {
            console.log('Hey! What\'s up? I\'m ' + this.name + ', I \'m a ' + this.job + ' and I\'m ' + this.age + ' years old. Have a nice ' + timeOfDay + '!');
        }
    }
}

var emily = {
    name: 'Emily',
    age: 35,
    job: 'designer'
}

john.presentation('formal', 'morning');

john.presentation.call(emily, 'friendly', 'afternoon');

//john.presentation.apply(emily, ['friendly', 'afternoon']);

var johnFriendly = john.presentation.bind(john, 'friendly');

johnFriendly('morning');
johnFriendly('night');

var emilyFormal = john.presentation.bind(emily, 'formal');
emilyFormal('afternoon'); */

/* var years = [1990, 1965, 1937, 2005, 1998];

function arrayCalc(arr, fn) {
    var arrRes = [];
    for (var i = 0; i < arr.length; i++) {
        arrRes.push(fn(arr[i]));
    }
    return arrRes;
}

function isFullAge(limit, el) {
    return el >= limit;
}

function calculateAge(el) {
    return 2016 - el;
}

var ages = arrayCalc(years, calculateAge);
var fullJapan = arrayCalc(ages, isFullAge.bind(this, 20));
console.log(ages);
console.log(fullJapan); */

/* (function () {
    var Question = function (question, answers, correctAnswer) {
        this.question = question;
        this.answers = answers;
        this.correctAnswer = correctAnswer;
    };

    Question.prototype.printQuestion = function () {
        console.log(this.question);
        for (var i = 0; i < this.answers.length; i++) {
            console.log(i + '. ' + this.answers[i]);
        }
    }

    Question.prototype.testAnswer = function (answer) {
        if (this.correctAnswer === answer) {
            console.log('Correct!');
        } else {
            console.log('Wrong');
        }

        console.log('------------------');
    }

    var question1 = new Question('a?', ['a', 'b', 'c'], 0);
    var question2 = new Question('b?', ['a', 'b', 'c', 'd'], 1);
    var question3 = new Question('c?', ['a', 'b', 'c', 'd', 'e'], 2);

    var questions = [question1, question2, question3];

    var randomQuestion = Math.floor(Math.random() * questions.length);
    var question = questions[randomQuestion];
    question.printQuestion();
    var answer = parseInt(prompt(questions[randomQuestion].question));

    while (answer !== 'exit') {
        question.testAnswer(answer);
        var randomQuestion = Math.floor(Math.random() * questions.length);
        var question = questions[randomQuestion];
        question.printQuestion();
        var answer = parseInt(prompt(questions[randomQuestion].question));        
    }
})(); */

(function () {
    var Question = function (question, answers, correctAnswer) {
        this.question = question;
        this.answers = answers;
        this.correctAnswer = correctAnswer;
    };

    Question.prototype.printQuestion = function () {
        console.log(this.question);
        for (var i = 0; i < this.answers.length; i++) {
            console.log(i + '. ' + this.answers[i]);
        }
    }

    Question.prototype.testAnswer = function (answer, callback) {
        var sc;

        if (this.correctAnswer === answer) {
            console.log('Correct!');
            sc = callback(true);
        } else {
            console.log('Wrong');
            sc = callback(false);
        }

        this.displayScore(sc);
    }

    Question.prototype.displayScore = function(score) {
        console.log('Your current score is: ' + score);
        console.log('------------');
    }

    var question1 = new Question('a?', ['a', 'b', 'c'], 0);
    var question2 = new Question('b?', ['a', 'b', 'c', 'd'], 1);
    var question3 = new Question('c?', ['a', 'b', 'c', 'd', 'e'], 2);

    var questions = [question1, question2, question3];

    function score() {
        var sc = 0;
        return function(correct) {
            if (correct) {
                sc++;
            }
            return sc;
        }
    }

    var keepScore = score();

    function nextQuestion() {
        var randomQuestion = Math.floor(Math.random() * questions.length);
        var question = questions[randomQuestion];
        question.printQuestion();
        var answer = prompt(questions[randomQuestion].question);        

        if(answer !== 'exit') {
            question.testAnswer(parseInt(answer), keepScore);
            nextQuestion();
        }
    }    
    
    nextQuestion();

})();

